'''
Building Example in Hylaa. This is the building example from 

ARCH-COMP19 Category Report: Continuous and Hybrid Systems with Linear Continuous Dynamics

originally from

H.-D. Tran, L. V. Nguyen, and T. T. Johnson. Large-scale linear systems from order-reduction. In
Proc. of ARCH16. 3rd International Workshop on Applied Verification for Continuous and Hybrid
Systems, 2017.
'''

import sys
import time

import numpy as np
from scipy.io import loadmat
from scipy.sparse import csr_matrix, csc_matrix

from matplotlib import collections

from hylaa.hybrid_automaton import HybridAutomaton
from hylaa.settings import HylaaSettings, PlotSettings
from hylaa.core import Core
from hylaa.stateset import StateSet
from hylaa import lputil

def make_automaton(limit):
    '''make the hybrid automaton and return it'''

    ha = HybridAutomaton()

    mode = ha.new_mode('mode')
    dynamics = loadmat('build.mat')
    a_matrix = dynamics['A']
    b_matrix = csc_matrix(dynamics['B'])
    n = a_matrix.shape[1]

    # add the input as a state variable
    big_a_mat = np.zeros((n+1, n+1))
    big_a_mat[0:n, 0:n] = a_matrix.toarray()
    big_a_mat[0:n, n:n+1] = b_matrix.toarray()
    a_matrix = big_a_mat

    mode.set_dynamics(csr_matrix(big_a_mat))

    error = ha.new_mode('error')

    # add an extra entry to the y matrix
    big_y = np.zeros((n+1,))
    big_y[:n] = dynamics['C'][0]
    mat = csr_matrix(big_y, dtype=float)

    trans1 = ha.new_transition(mode, error)
    rhs = np.array([-limit], dtype=float) # safe
    trans1.set_guard(-1 * mat, rhs) # y3 >= limit

    return ha

def make_init(ha):
    '''returns list of initial states'''

    bounds_list = []

    dims = list(ha.modes.values())[0].a_csr.shape[0]

    for dim in range(dims):
        if dim < 10:
            lb = 0.0002
            ub = 0.00025
        elif dim == 25:
            lb = -0.0001
            ub = 0.0001
        elif dim == 48: # affine input effects term
            lb = 0.8
            ub = 1.0
        else:
            lb = ub = 0

        bounds_list.append((lb, ub))

    mode = ha.modes['mode']
    init_lpi = lputil.from_box(bounds_list, mode)

    init_list = [StateSet(init_lpi, mode)]

    return init_list

def make_settings(lines=None, filename=None, short=False):
    'get the hylaa settings object'

    step = 0.01
    max_time = 20.0 if not short else 1.0
    settings = HylaaSettings(step, max_time)

    plot_settings = settings.plot
    plot_settings.plot_mode = PlotSettings.PLOT_NONE

    if filename is not None:
        y1 = loadmat('build.mat')['C'][0]
        big_y = np.zeros((y1.shape[0]+1,))
        big_y[:y1.shape[0]] = y1

        plot_settings.xdim_dir = None
        plot_settings.ydim_dir = big_y

        plot_settings.label.y_label = '$x_{25}$'
        plot_settings.label.x_label = '$t$'
        plot_settings.label.title = ''
        plot_settings.plot_size = (10, 10)
   
        settings.stdout = HylaaSettings.STDOUT_VERBOSE
        settings.plot.filename = filename
        settings.plot.plot_mode = PlotSettings.PLOT_IMAGE

        # add lines
        line_pts = []
        
        for line in lines:
            tmax = settings.num_steps * settings.step_size
            
            line_pts.append([(0, line), (tmax, line)])
            
        c1 = collections.LineCollection(line_pts, animated=True, colors=('red'), linewidths=(2), linestyle='dotted')
        
        settings.plot.extra_collections = [[c1]]

    return settings

def run_hylaa():
    'Runs hylaa with the given settings'

    name = 'BLDC01'
    safe_limit = 5.1e-3
    unsafe_limit = 4.0e-3

    if len(sys.argv) > 1 and sys.argv[1] == 'safe':
        ha = make_automaton(limit=safe_limit)
        init_states = make_init(ha)
        settings = make_settings()

        start = time.time()
        res = Core(ha, settings).run(init_states)
        assert not res.has_concrete_error
        
        print(f"Safe Result:\n{name}_S01 - {time.time() - start:.2f}")
    elif len(sys.argv) > 1 and sys.argv[1] == 'unsafe':
        ha = make_automaton(limit=unsafe_limit)
        init_states = make_init(ha)
        settings = make_settings()

        start = time.time()
        res = Core(ha, settings).run(init_states)
        assert res.has_concrete_error
        
        print(f"Unsafe Result:\n{name}_U01 - {time.time() - start:.2f}")
    elif len(sys.argv) > 1 and sys.argv[1] == 'plot':
        ha = make_automaton(limit=safe_limit)
        init_states = make_init(ha)
        lines = [safe_limit, unsafe_limit]
        settings = make_settings(lines, filename=f'{name}.png')
        
        Core(ha, settings).run(init_states)
    elif len(sys.argv) > 1 and sys.argv[1] == 'plot_short':
        ha = make_automaton(limit=safe_limit)
        init_states = make_init(ha)
        lines = [safe_limit, unsafe_limit]
        settings = make_settings(lines, filename=f'{name}_short.png', short=True)
        
        Core(ha, settings).run(init_states)
    else:
        print("expected single argument: safe/unsafe/plot")

if __name__ == '__main__':
    run_hylaa()
