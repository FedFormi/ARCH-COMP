\contentsline {section}{\numberline {1}What's new compared to CORA 2016?}{4}{section.1}
\contentsline {section}{\numberline {2}Philosophy and Architecture}{4}{section.2}
\contentsline {section}{\numberline {3}Installation}{5}{section.3}
\contentsline {section}{\numberline {4}Connections to and from SpaceEx}{6}{section.4}
\contentsline {subsection}{\numberline {4.1}Importing SpaceEx Models}{6}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}CORA/SX}{6}{subsection.4.2}
\contentsline {section}{\numberline {5}Architecture}{6}{section.5}
\contentsline {section}{\numberline {6}Set Representations and Operations}{7}{section.6}
\contentsline {subsection}{\numberline {6.1}Zonotopes}{8}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}Method \texttt {mtimes}}{10}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.2}Method \texttt {plus}}{10}{subsubsection.6.1.2}
\contentsline {subsubsection}{\numberline {6.1.3}Method \texttt {reduce}}{10}{subsubsection.6.1.3}
\contentsline {subsubsection}{\numberline {6.1.4}Method \texttt {split}}{10}{subsubsection.6.1.4}
\contentsline {subsubsection}{\numberline {6.1.5}Zonotope Example}{11}{subsubsection.6.1.5}
\contentsline {subsection}{\numberline {6.2}Zonotope Bundles}{12}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Zonotope Bundle Example}{14}{subsubsection.6.2.1}
\contentsline {subsection}{\numberline {6.3}Polynomial Zonotopes}{14}{subsection.6.3}
\contentsline {subsubsection}{\numberline {6.3.1}Method \texttt {reduce}}{17}{subsubsection.6.3.1}
\contentsline {subsubsection}{\numberline {6.3.2}Polynomial Zonotope Example}{17}{subsubsection.6.3.2}
\contentsline {subsection}{\numberline {6.4}Probabilistic Zonotopes}{18}{subsection.6.4}
\contentsline {subsubsection}{\numberline {6.4.1}Probabilistic Zonotope Example}{20}{subsubsection.6.4.1}
\contentsline {subsection}{\numberline {6.5}Constrained Zonotopes}{20}{subsection.6.5}
\contentsline {subsubsection}{\numberline {6.5.1}Method \texttt {and}}{21}{subsubsection.6.5.1}
\contentsline {subsubsection}{\numberline {6.5.2}Method \texttt {enclose}}{21}{subsubsection.6.5.2}
\contentsline {subsubsection}{\numberline {6.5.3}Method \texttt {plus}}{22}{subsubsection.6.5.3}
\contentsline {subsubsection}{\numberline {6.5.4}Method \texttt {reduce}}{22}{subsubsection.6.5.4}
\contentsline {subsubsection}{\numberline {6.5.5}Constrained Zonotope Example}{22}{subsubsection.6.5.5}
\contentsline {subsection}{\numberline {6.6}MPT Polytopes}{23}{subsection.6.6}
\contentsline {paragraph}{H-Representation of a Polytope}{24}{subsection.6.6}
\contentsline {paragraph}{V-Representation of a Polytope}{24}{subsection.6.6}
\contentsline {subsubsection}{\numberline {6.6.1}MPT Polytope Example}{25}{subsubsection.6.6.1}
\contentsline {subsection}{\numberline {6.7}Intervals}{26}{subsection.6.7}
\contentsline {paragraph}{Methods realizing mathematical functions and operations}{26}{subsection.6.7}
\contentsline {paragraph}{Other methods}{27}{subsection.6.7}
\contentsline {subsubsection}{\numberline {6.7.1}Interval Example}{28}{subsubsection.6.7.1}
\contentsline {subsection}{\numberline {6.8}Taylor Models}{29}{subsection.6.8}
\contentsline {subsubsection}{\numberline {6.8.1}Initialization}{30}{subsubsection.6.8.1}
\contentsline {subsubsection}{\numberline {6.8.2}Mathematical operations}{31}{subsubsection.6.8.2}
\contentsline {subsubsection}{\numberline {6.8.3}Bound estimators}{33}{subsubsection.6.8.3}
\contentsline {subsubsection}{\numberline {6.8.4}Interval substitution}{33}{subsubsection.6.8.4}
\contentsline {subsubsection}{\numberline {6.8.5}Branch and bound}{34}{subsubsection.6.8.5}
\contentsline {subsubsection}{\numberline {6.8.6}Small coefficients pruning}{34}{subsubsection.6.8.6}
\contentsline {subsubsection}{\numberline {6.8.7}List of the functions realized in CORA}{34}{subsubsection.6.8.7}
\contentsline {subsubsection}{\numberline {6.8.8}Taylor Model Example}{34}{subsubsection.6.8.8}
\contentsline {subsection}{\numberline {6.9}Vertices}{34}{subsection.6.9}
\contentsline {subsubsection}{\numberline {6.9.1}Vertices Example}{35}{subsubsection.6.9.1}
\contentsline {subsection}{\numberline {6.10}Plotting of Sets}{36}{subsection.6.10}
\contentsline {section}{\numberline {7}Matrix Set Representations and Operations}{37}{section.7}
\contentsline {subsection}{\numberline {7.1}Matrix Polytopes}{38}{subsection.7.1}
\contentsline {subsubsection}{\numberline {7.1.1}Matrix Polytope Example}{39}{subsubsection.7.1.1}
\contentsline {subsection}{\numberline {7.2}Matrix Zonotopes}{41}{subsection.7.2}
\contentsline {subsubsection}{\numberline {7.2.1}Matrix Zonotope Example}{42}{subsubsection.7.2.1}
\contentsline {subsection}{\numberline {7.3}Interval Matrices}{43}{subsection.7.3}
\contentsline {subsubsection}{\numberline {7.3.1}Interval Matrix Example}{45}{subsubsection.7.3.1}
\contentsline {section}{\numberline {8}Continuous Dynamics}{46}{section.8}
\contentsline {subsection}{\numberline {8.1}Linear Systems}{47}{subsection.8.1}
\contentsline {subsubsection}{\numberline {8.1.1}Method \texttt {initReach}}{47}{subsubsection.8.1.1}
\contentsline {subsection}{\numberline {8.2}Linear Systems with Uncertain Fixed Parameters}{48}{subsection.8.2}
\contentsline {subsubsection}{\numberline {8.2.1}Method \texttt {initReach}}{49}{subsubsection.8.2.1}
\contentsline {subsection}{\numberline {8.3}Linear Systems with Uncertain Varying Parameters}{49}{subsection.8.3}
\contentsline {subsection}{\numberline {8.4}Linear Probabilistic Systems}{50}{subsection.8.4}
\contentsline {subsubsection}{\numberline {8.4.1}Method \texttt {initReach}}{50}{subsubsection.8.4.1}
\contentsline {subsection}{\numberline {8.5}Nonlinear Systems}{51}{subsection.8.5}
\contentsline {subsubsection}{\numberline {8.5.1}Method \texttt {initReach}}{52}{subsubsection.8.5.1}
\contentsline {subsection}{\numberline {8.6}Nonlinear Systems with Uncertain Fixed Parameters}{53}{subsection.8.6}
\contentsline {subsection}{\numberline {8.7}Nonlinear Differential-Algebraic Systems}{53}{subsection.8.7}
\contentsline {section}{\numberline {9}Hybrid Dynamics}{54}{section.9}
\contentsline {subsection}{\numberline {9.1}Simulation of Hybrid Automata}{55}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Hybrid Automaton}{56}{subsection.9.2}
\contentsline {subsection}{\numberline {9.3}Location}{56}{subsection.9.3}
\contentsline {subsection}{\numberline {9.4}Transition}{58}{subsection.9.4}
\contentsline {section}{\numberline {10}State Space Partitioning}{58}{section.10}
\contentsline {subsection}{\numberline {10.1}Migrating the old \texttt {partition} Class into the new one}{59}{subsection.10.1}
\contentsline {section}{\numberline {11}Options for Reachability Analysis}{59}{section.11}
\contentsline {section}{\numberline {12}Unit Tests}{60}{section.12}
\contentsline {section}{\numberline {13}Loading Simulink and SpaceEx Models}{60}{section.13}
\contentsline {subsection}{\numberline {13.1}Converting Simulink Models to SpaceEx Models}{60}{subsection.13.1}
\contentsline {subsection}{\numberline {13.2}Loading SpaceEx Models}{60}{subsection.13.2}
\contentsline {section}{\numberline {14}Examples}{62}{section.14}
\contentsline {subsection}{\numberline {14.1}Continuous Dynamics}{62}{subsection.14.1}
\contentsline {subsubsection}{\numberline {14.1.1}Linear Dynamics}{62}{subsubsection.14.1.1}
\contentsline {subsubsection}{\numberline {14.1.2}Linear Dynamics with Uncertain Parameters}{64}{subsubsection.14.1.2}
\contentsline {subsubsection}{\numberline {14.1.3}Nonlinear Dynamics}{67}{subsubsection.14.1.3}
\contentsline {paragraph}{Tank System}{67}{subsubsection.14.1.3}
\contentsline {paragraph}{Van der Pol Oscillator}{70}{figure.caption.35}
\contentsline {paragraph}{Seven-Dimensional Example for Non-Convex Set Representation}{70}{figure.caption.36}
\contentsline {paragraph}{Autonomous Car Following a Reference Trajectory}{70}{figure.caption.37}
\contentsline {subsubsection}{\numberline {14.1.4}Nonlinear Dynamics with Uncertain Parameters}{71}{subsubsection.14.1.4}
\contentsline {subsubsection}{\numberline {14.1.5}Nonlinear Differential-Algebraic Systems}{74}{subsubsection.14.1.5}
\contentsline {subsection}{\numberline {14.2}Hybrid Dynamics}{76}{subsection.14.2}
\contentsline {subsubsection}{\numberline {14.2.1}Bouncing Ball Example}{76}{subsubsection.14.2.1}
\contentsline {subsubsection}{\numberline {14.2.2}Powertrain Example}{79}{subsubsection.14.2.2}
\contentsline {section}{\numberline {15}Conclusions}{79}{section.15}
\contentsline {section}{\numberline {A}Migrating the \texttt {intervalhull} Class into the \texttt {interval} Class}{80}{appendix.A}
\contentsline {section}{\numberline {B}Implementation of Loading SpaceEx Models}{80}{appendix.B}
\contentsline {subsection}{\numberline {B.1}Preface: The SpaceEx Format}{80}{subsection.B.1}
\contentsline {subsection}{\numberline {B.2}Parsing The Component Definitions}{81}{subsection.B.2}
\contentsline {subsubsection}{\numberline {B.2.1}Accessing XML Files}{81}{subsubsection.B.2.1}
\contentsline {subsubsection}{\numberline {B.2.2}Storing The Component Definitions}{81}{subsubsection.B.2.2}
\contentsline {subsection}{\numberline {B.3}"Instantiating" the Component Tree}{83}{subsection.B.3}
\contentsline {subsection}{\numberline {B.4}Forming the Parallel Composition}{84}{subsection.B.4}
\contentsline {subsubsection}{\numberline {B.4.1}Combining Flow Functions}{84}{subsubsection.B.4.1}
\contentsline {subsubsection}{\numberline {B.4.2}Combining Invariants}{85}{subsubsection.B.4.2}
\contentsline {subsubsection}{\numberline {B.4.3}Combining Transitions}{85}{subsubsection.B.4.3}
\contentsline {subsection}{\numberline {B.5}Formalizing Symbolic Dynamics}{86}{subsection.B.5}
\contentsline {subsubsection}{\numberline {B.5.1}Linear \& Non-linear Flows}{86}{subsubsection.B.5.1}
\contentsline {subsubsection}{\numberline {B.5.2}Polyhedric Guard \& Invariant Sets}{86}{subsubsection.B.5.2}
\contentsline {subsection}{\numberline {B.6}Conversion to MATLAB Function}{87}{subsection.B.6}
\contentsline {subsection}{\numberline {B.7}Open Problems}{87}{subsection.B.7}
\contentsline {section}{\numberline {C}Licensing}{87}{appendix.C}
\contentsline {section}{\numberline {D}Disclaimer}{87}{appendix.D}
\contentsline {section}{\numberline {E}Contributors}{88}{appendix.E}
