function [automaton, numberOfComp,numberOfBinds] = ComputeSystem( h_sys, h_base, h_net )
%INPUT: 
%   h_sys:
%   h_base: list of base component structures
%   h_net: list of network component structures
%
%OUTPUT:
%   automaton: a (possible parallel) hybrid automaton
%   numberOfComp: number of components
%   numberOfBinds: number of binds
%


if nargin ==1
    h_net = struct([]);
    h_base = struct([]);
elseif nargin == 2
    h_net = struct([]);
elseif nargin ~= 3
    error('Input argument missing in ComputeSys');
end
[listOfVar, listOfLab] = CollectVariables(h_sys.param);

 
numberOfComp = 0;
numberOfBinds = 0;

if isfield(h_sys, 'bind')
    N = length(h_sys.bind);
    
    %compute total number of components
    for i = 1:N
        template{i} = h_sys.bind{i}.Attributes.component;
        %search template in base components
        for j = 1:length(h_base)
            if strcmp(h_base{j}.id,template{i})
                h_base{j} = AssignTemplate(h_base{j},h_sys.bind{i},listOfVar,listOfLab);
                numberOfComp = numberOfComp +1;
            end
        end
        %search template in network componets
        for j = 1: length(h_net)
            if strcmp(h_net{j}.id,template{i})
                h_net{j} = AssignTemplate(h_net{j},h_sys.bind{i},listOfVar,listOfLab);
                numberOfComp = numberOfComp + h_net{j}.numberOfComp;
            end
        end
    end
else
    numberOfComp = 1;
    template{1} = h_sys.Attributes.id;
end

for i = 1:numberOfComp
    currentTemplateName = template{i};
    %search template in base components
    for j = 1:length(h_base)
        if strcmp(h_base{j}.id,template{i})
             h_base{j} = FormalizeBaseComponent(h_base{j});
             automaton.Components{i}.id = h_base{j}.id;
             automaton.Components{i}.numberOfStates = h_base{j}.numberOfStates;
             automaton.Components{i}.States = h_base{j}.States;
             automaton.Components{i}.states = h_base{j}.states;
             automaton.Components{i}.inputs = h_base{j}.inputs;
        end
    end
    %search template in network componets
    for j = 1: length(h_net)
        if strcmp(h_net{j}.id,template{i})
            automaton.Components{i}.id = h_net{j}.id;
            %count number of States in Components
            for k = 1:length(h_net{j}.Components)
                 automaton.Components{i}.numberOfStates = h_net{j}.Components{k}.numberOfStates;
            end
             
        end
    end
end
%TODO(parallel automata): assign Binds
automaton.Binds = struct([]);

end



