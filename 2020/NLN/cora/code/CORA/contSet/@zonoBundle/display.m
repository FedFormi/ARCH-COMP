function display(obj)
% display - Displays a zonotope bundle
%
% Syntax:  
%    display(obj)
%
% Inputs:
%    Z - zonotope bundle
%
% Outputs:
%    ---
%
% Example: 
%    ---
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff
% Written:      09-November-2010
% Last update:  02-May-2020 (MW, add empty case)
% Last revision:---

%------------- BEGIN CODE --------------

%display each zonotope
if obj.parallelSets == 0
    dispEmptyObj(obj,inputname(1));
    
else
    for i=1:obj.parallelSets
        disp(['zonotope ',num2str(i),':']);
        display(obj.Z{i});
    end
end

%------------- END OF CODE --------------