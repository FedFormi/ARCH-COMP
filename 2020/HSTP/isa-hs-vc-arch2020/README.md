Instructions to obtain results of the ARCH2020 competition for the Isabelle/HOL verification components:
1. Grant permissions to "RUNME.sh", e.g. `$ chmod +x RUNME.sh`
2. Run the "RUNME.sh" script, e.g. `$ ./RUNME.sh`

After waiting for LaTeX and Isabelle to build in the container, open the formalisation of the problems in the Isabelle-generated "document.pdf".

**NOTE:** Each problem corresponds to a subsection of the pdf. Problems partially proved include the “sorry” command. Problems that we did not manage to tackle during the time frame of the competition include an “oops” command.