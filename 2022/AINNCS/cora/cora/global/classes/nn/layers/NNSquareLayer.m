classdef NNSquareLayer < NNLayer
    % NNSquareLayer - class for square layers
    %
    % Syntax:
    %    obj = NNSquareLayer(name)
    %
    % Inputs:
    %    name - name of the layer, defaults to type
    %
    % Outputs:
    %    obj - generated object
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    % See also: NeuralNetwork
    %
    % Author:       Tobias Ladner
    % Written:      04-July-2022
    % Last update:  ---
    % Last revision:---

    %------------- BEGIN CODE --------------

    properties (Constant)
        type = "SquareLayer"
        is_refinable = false
    end

    properties
        W, b
    end

    methods
        % constructor
        function obj = NNSquareLayer(name)
            if nargin < 1
                name = NNLinearLayer.type;
            end
            % call super class constructor
            obj@NNLayer(name)
        end

        function [nin, nout] = get_num_neurons(obj)
            nin = size(obj.W, 2);
            nout = size(obj.W, 1);
        end

        % evaluate
        function r = evaluate_numeric(obj, input)
            r = input .^ 2;
        end

        function r = evaluateZonotope(obj, Z, evParams)
            error("Not Implemented!")
        end

        function [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotope(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
            
            [c, G, Grest] = NNHelper.calcSquared(c, G, Grest, expMat, c, G, Grest, expMat, true);
            
            
            expMat = NNHelper.calcSquaredE(expMat, expMat, true);

            ind = find(prod(ones(size(expMat))- ...
                mod(expMat, 2), 1) == 1);
            ind_ = setdiff(1:size(expMat, 2), ind);
        end

        function r = evaluateTaylm(obj, input, evParams)
            error("Not Implemented!")
        end

        function [c, G, C, d, l, u] = evaluateConZonotope(obj, c, G, C, d, l, u, options, evParams)
            error("Not Implemented!")
        end

        function S = evaluate_sensitivity(obj, S, x)
            error("Not Implemented!")
        end
    end
end