classdef NNApproximationLayer < NNActivationLayer
    % NNApproximationLayer - class for approximating multiple layers
    %
    % Syntax:
    %    obj = NNReLULayer(name)
    %
    % Inputs:
    %    name - name of the layer, defaults to type
    %
    % Outputs:
    %    obj - generated object
    %
    % References:
    %    [1] Tran, H.-D., et al. "Star-Based Reachability Analysis of Deep
    %        Neural Networks", 2019
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    % See also: NeuralNetwork
    %
    % Author:       Tobias Ladner
    % Written:      28-March-2022
    % Last update:  ---
    % Last revision:---

    %------------- BEGIN CODE --------------

    properties (Constant)
        type = "NNApproximationLayer"
    end

    properties
        layers(:, 1) = []
        sym_dfs = cell(0, 1)
    end

    methods
        % constructor
        function obj = NNApproximationLayer(layers, name)
            if nargin < 3
                names = cellfun(@(layer_i) string(layer_i.name), layers);
                name = strtrim(sprintf("%s ", names));
            end
            % call super class constructor
            obj@NNActivationLayer(name)

            obj.layers = layers;

            % re-init function handles
            obj.sym_dfs = cell(0, 1);
            obj.dfs = cell(0, 1);

            obj.f = @(x) obj.evaluate_numeric(x);
            obj.df = obj.get_df(1);

            max_df_init = 6;
            obj.dfs = cell(max_df_init, 1);
            for i = 1:max_df_init
                obj.dfs{i} = obj.get_df(i);
            end
        end

        function df_i = get_df(obj, i)
            if i == 0
                df_i = obj.f;
            else
                % TODO might need rework
                df_i = @(x) 1;
            end
        end

        function [nin, nout] = get_num_neurons(obj)
            for i = 1:size(obj.layers, 1)
                [nin, ~] = obj.layers{i}.get_num_neurons();
                if ~isempty(nin)
                    break;
                end
            end

            for i = size(obj.layers, 1):-1:1
                [~, nout] = obj.layers{i}.get_num_neurons();
                if ~isempty(nout)
                    break;
                end
            end
        end

        % evaluate
        function r = evaluate_numeric(obj, input)
            r = input;
            for j = 1:size(obj.layers, 1)
                r = obj.layers{j, 1}.evaluate_numeric(r);
            end
        end

        function [res, d] = evaluate_zonotope_neuron(obj, input)
            error("Not Implemented!")
        end

        function [c, G, Grest, d] = evaluate_polyZonotope_neuron_lin(c, G, Grest, E, ind, ind_, approx)
            error("Not Implemented!")
        end

        function [c, G, Grest, d] = evaluate_polyZonotope_neuron_quad(obj, c, G, Grest, E, ind, ind_, approx)
            error("Not Implemented!")
        end

        function [c, G, Grest, d] = evaluate_polyZonotope_neuron_cub(obj, c, G, Grest, E, ind, ind_, approx)
            error("Not Implemented!")
        end

        [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotopeAdaptive(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
        [c, G, Grest, d] = evaluatePolyZonotopeAdaptiveNeuron(obj, c, G, Grest, Es, ind, ind_, approx)

        function r = evaluate_taylm_neuron(obj, input, evParams)
            error("Not Implemented!")
        end

        function [c, G, C, d, l_, u_] = evaluate_conZonotope_neuron(obj, c, G, C, d, l_, u_, j, options, evParams)
            error("Not Implemented!")
        end
    end
end