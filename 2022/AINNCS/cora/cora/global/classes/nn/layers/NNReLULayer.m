classdef NNReLULayer < NNActivationLayer
    % NNReLULayer - class for ReLU layers
    %
    % Syntax:
    %    obj = NNReLULayer(name)
    %
    % Inputs:
    %    name - name of the layer, defaults to type
    %
    % Outputs:
    %    obj - generated object
    %
    % References:
    %    [1] Tran, H.-D., et al. "Star-Based Reachability Analysis of Deep
    %        Neural Networks", 2019
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    % See also: NeuralNetwork
    %
    % Author:       Tobias Ladner
    % Written:      28-March-2022
    % Last update:  ---
    % Last revision:---

    %------------- BEGIN CODE --------------

    properties (Constant)
        type = "ReLULayer"
    end

    methods
        % constructor
        function obj = NNReLULayer(name)
            if nargin < 2
                name = NNReLULayer.type;
            end
            % call super class constructor
            obj@NNActivationLayer(name)
        end

        function df_i = get_df(obj, i)
            if i == 0
                df_i = obj.f;
            elseif i == 1
                df_i = @(x) 1 * (x > 0);
            else
                df_i = @(x) 0;
            end
        end

        function der1 = getDerBounds(obj, l, u)
            % df_l and df_u as lower and upper bound for the derivative
            % case distinction for l
            if l <= 0
                df_l = 0;
            else
                df_l = 1;
            end

            % case distinction for u
            if u <= 0
                df_u = 0;
            else
                df_u = 1;
            end
            der1 = interval(df_l, df_u);
        end

        % evaluate
        function r = evaluate_numeric(obj, input)
            r = max(0, input);
        end

        function [res, d] = evaluate_zonotope_neuron(obj, input)
            % enclose the ReLU activation function with a zonotope according to
            % Theorem 3.1 in [1]

            % compute lower and upper bound
            l = input(1) - sum(abs(input(2:end)));
            u = input(1) + sum(abs(input(2:end)));

            % compute output
            if u <= 0
                res = zeros(size(input));
                d = 0;
            elseif l > 0
                res = input;
                d = 0;
            else
                lambda = u / (u - l);
                mu = -(u * l) / (2 * (u - l));
                res = lambda * input;
                res(1) = res(1) + mu;
                d = mu;
            end
        end

        function [c, G, Grest, d] = evaluate_polyZonotope_neuron_lin(obj, c, G, Grest, E, ind, ind_, approx)
            % enclose the ReLU activation function with a polynomial zonotope according
            % to Theorem 3.1 in [1]

            % compute lower and upper bound
            [l, u] = NNHelper.compBoundsPolyZono(c, G, Grest, E, ind, ind_, approx);

            % compute output
            if u <= 0
                c = 0;
                G = zeros(size(G));
                Grest = zeros(size(Grest));
                d = 0;
            elseif l > 0
                d = 0;
            else
                lambda = u / (u - l);
                mu = -(u * l) / (2 * (u - l));
                c = lambda * c + mu;
                G = lambda * G;
                Grest = lambda * Grest;
                d = mu;
            end
        end

        function [c, G, Grest, d] = evaluate_polyZonotope_neuron_quad(obj, c, G, Grest, E, ind, ind_, approx)
            % enclose the ReLU activation function with a polynomial zonotope by
            % fitting a quadratic function

            % properties
            h = length(G);
            q = length(Grest);

            % compute lower and upper bound
            [l, u] = NNHelper.compBoundsPolyZono(c, G, Grest, E, ind, ind_, approx);

            % compute output
            if u <= 0
                c = 0;
                G = zeros(1, h+0.5*(h^2 + h));
                Grest = zeros(1, q+q*h+0.5*(q^2 + q));
                d = 0;
            elseif l > 0
                G = [G, zeros(1, 0.5*(h^2 + h))];
                Grest = [Grest, zeros(1, q*h+0.5*(q^2 + q))];
                d = 0;
            else

                % compute quadratic function that best fits the activation function
                c_a = u / (u - l)^2;
                c_b = -2 * l * u / (u - l)^2;
                c_c = u^2 * (2 * l - u) / (u - l)^2 + u;

                % compute difference between ReLU and quadratic approximation
                L1 = NNHelper.minMaxQuadFun(-c_a, 1-c_b, -c_c, 0, u);
                L2 = NNHelper.minMaxQuadFun(-c_a, -c_b, -c_c, l, 0);
                L = L1 | L2;
                d = rad(L);

                % evaluate the quadratic approximation on the polynomial zonotope
                [c, G, Grest] = NNHelper.quadApproxPolyZono(c, G, Grest, c_a, c_b, c_c);
                c = c + center(L);
            end
        end

        function [c, G, Grest, d] = evaluate_polyZonotope_neuron_cub(obj, c, G, Grest, E, ind, ind_, approx)
            % enclose the ReLU activation function with a polynomial zonotope by
            % fitting a cubic function

            % properties
            h = length(G);
            q = length(Grest);

            % compute lower and upper bound
            [l, u] = NNHelper.compBoundsPolyZono(c, G, Grest, E, ind, ind_, approx);

            % compute output
            if u <= 0
                c = 0;
                G = zeros(1, nchoosek(3+h, h)-1);
                temp = nchoosek(3+q, q) - 1 + h * q + 0.5 * (h^2 + h) * q + 0.5 * (q^2 + q) * h;
                Grest = zeros(1, temp);
                d = 0;
            elseif l > 0
                G = [G, zeros(1, nchoosek(3+h, h)-1-h)];
                temp = nchoosek(3+q, q) - 1 + h * q + 0.5 * (h^2 + h) * q + 0.5 * (q^2 + q) * h - q;
                Grest = [Grest, zeros(1, temp)];
                d = 0;
            else

                % compute quadratic function that best fits the activation function
                x = linspace(l, u, 10);
                y = max(0*x, x);
                temp = NNHelper.leastSquarePolyFunc(x, y, 3);
                c_d = temp(1);
                c_c = temp(2);
                c_b = temp(3);
                c_a = temp(4);

                % compute difference between ReLU and cubic approximation
                L1 = NNHelper.minMaxCubFun(-c_a, -c_b, 1-c_c, -c_d, 0, u);
                L2 = NNHelper.minMaxCubFun(-c_a, -c_b, -c_c, -c_d, l, 0);
                L = L1 | L2;
                d = rad(L);

                % evaluate the cubic approximation on the polynomial zonotope
                [c, G, Grest] = NNHelper.cubApproxPolyZono(c, G, Grest, c_a, c_b, c_c, c_d);
                c = c + center(L);
            end
        end

        function r = evaluate_taylm_neuron(obj, input, evParams)
            % enclose the ReLU activation function with a Taylor model by
            % fitting a quadratic function

            % compute lower and upper bound
            int = interval(input);
            l = infimum(int);
            u = supremum(int);

            % compute output
            if u <= 0
                r = 0 * input;
            elseif l > 0
                r = input;
            else

                if strcmp(evParams.polynomial_approx, 'lin')

                    % compute linear enclosure according to Theorem 3.1 in [1]
                    lambda = u / (u - l);
                    mu = -(u * l) / (2 * (u - l));
                    r = lambda * input + interval(0, 2*mu);

                elseif strcmp(evParams.polynomial_approx, 'quad')

                    % compute quadratic fit to the activation function
                    x = linspace(l, u, 10);
                    y = max(0*x, x);
                    temp = NNHelper.leastSquarePolyFunc(x, y, 2);
                    c_c = temp(1);
                    c_b = temp(2);
                    c_a = temp(3);

                    % compute difference between ReLU and quadratic approximation
                    L1 = NNHelper.minMaxQuadFun(-c_a, 1-c_b, -c_c, 0, u);
                    L2 = NNHelper.minMaxQuadFun(-c_a, -c_b, -c_c, l, 0);
                    L = L1 | L2;

                    % compute resulting Taylor model
                    r = c_a * input^2 + c_b * input + c_c + L;

                elseif strcmp(evParams.polynomial_approx, 'cub')

                    % compute cubic fit to the activation function
                    x = linspace(l, u, 10);
                    y = max(0*x, x);
                    temp = NNHelper.leastSquarePolyFunc(x, y, 3);
                    c_d = temp(1);
                    c_c = temp(2);
                    c_b = temp(3);
                    c_a = temp(4);

                    % compute difference between ReLU and cubic approximation
                    L1 = NNHelper.minMaxCubFun(-c_a, -c_b, 1-c_c, -c_d, 0, u);
                    L2 = NNHelper.minMaxCubFun(-c_a, -c_b, -c_c, -c_d, l, 0);
                    L = L1 | L2;

                    % compute resulting Taylor model
                    r = c_a * input^3 + c_b * input^2 + c_c * input + c_d + L;
                end
            end
        end

        function [c, G, C, d, l_, u_] = evaluate_conZonotope_neuron(obj, c, G, C, d, l_, u_, j, options, evParams)
            % enclose the ReLU activation function with a constrained zonotope based on
            % the results for star sets in [1]

            n = length(c);
            m = size(G, 2);
            M = eye(n);
            M(:, j) = zeros(n, 1);

            % get lower bound
            if evParams.bound_approx
                c_ = c(j) + 0.5 * G(j, :) * (u_ - l_);
                G_ = 0.5 * G(j, :) * diag(u_-l_);
                l = c_ - sum(abs(G_));
            else
                [~, temp] = linprog(G(j, :), C, d, [], [], [], [], options);
                l = c(j) + temp;
            end

            % compute output according to Sec. 3.2 in [1]
            if l < 0

                % compute upper bound
                if evParams.bound_approx
                    u = c_ + sum(abs(G_));
                else
                    [~, temp] = linprog(-G(j, :), C, d, [], [], [], [], options);
                    u = c(j) - temp;
                end

                if u <= 0
                    c = M * c;
                    G = M * G;
                else
                    C1 = [zeros(1, m), -1];
                    d1 = 0;
                    C2 = [G(j, :), -1];
                    d2 = -c(j);
                    C3 = [-u / (u - l) * G(j, :), 1];
                    d3 = u / (u - l) * (c(j) - l);
                    C0 = [C, zeros(size(C, 1), 1)];
                    d0 = d;
                    C = [C0; C1; C2; C3];
                    d = [d0; d1; d2; d3];
                    temp = zeros(n, 1);
                    temp(j) = 1;
                    c = M * c;
                    G = M * G;
                    G = [G, temp];
                    l_ = [l_; 0];
                    u_ = [u_; u];
                end
            end
        end
    end
end