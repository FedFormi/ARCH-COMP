classdef NNElementwiseAffineLayer < NNLayer
    % NNElementwiseAffineLayer - class for elementwise affine layers
    %
    % Syntax:
    %    obj = NNElementwiseAffineLayer(scale, offset, name)
    %
    % Inputs:
    %    scale - elementwise scale
    %    offset - elementwise offset
    %    name - name of the layer, defaults to type
    %
    % Outputs:
    %    obj - generated object
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    % See also: NeuralNetwork
    %
    % Author:       Tobias Ladner
    % Written:      30-March-2022
    % Last update:  ---
    % Last revision:---

    %------------- BEGIN CODE --------------

    properties (Constant)
        type = "ElementwiseAffineLayer"
        is_refinable = false
    end

    properties
        scale, offset
    end

    methods
        % constructor
        function obj = NNElementwiseAffineLayer(scale, offset, name)
            if nargin < 3
                name = NNLinearLayer.type;
            end
            % call super class constructor
            obj@NNLayer(name)

            obj.scale = scale;
            obj.offset = offset;
        end

        function [nin, nout] = get_num_neurons(obj)
            nin = [];
            nout = [];
        end

        % evaluate
        function r = evaluate_numeric(obj, input)
            r = obj.scale .* input + obj.offset;
        end

        function r = evaluateZonotope(obj, Z, evParams)
            Z = obj.scale * Z;
            Z(:, 1) = Z(:, 1) + obj.offset;
            r = Z;
        end

        function [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotope(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
            c = obj.scale * c + obj.offset;
            G = obj.scale * G;
            Grest = obj.scale * Grest;
        end

        function r = evaluateTaylm(obj, input, evParams)
            r = obj.scale * input + obj.offset;
        end

        function [c, G, C, d, l, u] = evaluateConZonotope(obj, c, G, C, d, l, u, options, evParams)
            c = obj.scale * c + obj.offset;
            G = obj.scale * G;
        end

        function S = evaluate_sensitivity(obj, S, x)
            S = obj.scale .* S;
        end
    end
end