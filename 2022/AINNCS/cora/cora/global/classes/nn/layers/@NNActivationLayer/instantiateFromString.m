function layer = instantiateFromString(activation)
% creates an activation layer from string
%
% Syntax:
%    layer = NNActivationLayer.instantiateFromString(activation)
%
% Inputs:
%    activation - string
%
% Outputs:
%    layer - NNActivationLayer
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: NNLayer
%
% Author:        Tobias Ladner
% Written:       24-June-2022
% Last update:   ---
% Last revision: ---

%------------- BEGIN CODE --------------

if strcmp(activation, "ReLU")
    layer = NNReLULayer();
elseif strcmp(activation, "sigmoid")
    layer = NNSigmoidLayer();
elseif strcmp(activation, "tanh")
    layer = NNTanhLayer();
else
    error("Unkown activation '%s'", activation);
end

end