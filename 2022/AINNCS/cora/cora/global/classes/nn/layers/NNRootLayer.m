classdef NNRootLayer < NNActivationLayer
    % NNRootLayer - class for square root layers
    %
    % Syntax:
    %    obj = NNRootLayer(name)
    %
    % Inputs:
    %    name - name of the layer, defaults to type
    %
    % Outputs:
    %    obj - generated object
    %
    % References:
    %    -
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    % See also: NeuralNetwork
    %
    % Author:       Tobias Ladner
    % Written:      04-July-2022
    % Last update:  ---
    % Last revision:---

    %------------- BEGIN CODE --------------

    properties (Constant)
        type = "RootLayer"
    end

    methods
        % constructor
        function obj = NNRootLayer(name)
            if nargin < 2
                name = NNRootLayer.type;
            end
            % call super class constructor
            obj@NNActivationLayer(name)
        end

        function df_i = get_df(obj, i)
            if i == 0
                df_i = obj.f;
            else
                df_i1 = obj.get_df(i-1);
                df_i = @(x) 1/2 .* df_i1(x);
            end
        end

        function der1 = getDerBounds(obj, l, u)
            der1 = interval(obj.df(l), obj.df(u));
        end

        % evaluate
        function r = evaluate_numeric(obj, input)
            r = sqrt(input);
        end

        function [res, d] = evaluate_zonotope_neuron(obj, input)
            error("Not Implemented!")
        end

        function [c, G, Grest, d] = evaluate_polyZonotope_neuron_lin(obj, c, G, Grest, E, ind, ind_, approx)
            error("Not Implemented!")
        end

        function [c, G, Grest, d] = evaluate_polyZonotope_neuron_quad(obj, c, G, Grest, E, ind, ind_, approx)
            error("Not Implemented!")
        end

        function [c, G, Grest, d] = evaluate_polyZonotope_neuron_cub(obj, c, G, Grest, E, ind, ind_, approx)
            error("Not Implemented!")
        end

        function r = evaluate_taylm_neuron(obj, input, evParams)
            error("Not Implemented!")
        end

        function [c, G, C, d, l_, u_] = evaluate_conZonotope_neuron(obj, c, G, C, d, l_, u_, j, options, evParams)
            error("Not Implemented!")
        end
    end
end