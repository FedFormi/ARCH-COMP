function res = example_neuralNetwork_verify_01()
% example_neuralNetwork_verify_01 - example for neural network verification
%
% Syntax:  
%    res = example_neuralNetwork_verify_01()
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
% 
% Author:       Niklas Kochdumper
% Written:      17-December-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    % load neural network
    load('neuralNetworkReLU');

    % initial set
    X0 = interval([-1;-1],[1;1]);

    % specifications
    spec1 = specification( ...
                    mptPolytope(interval([2.5;0.2],[4;1.6])),'unsafeSet');
    spec2 = specification( ...
                       mptPolytope([-0.2 -0.1;4.1 -0.1;2 1.7]),'safeSet');

    spec = add(spec1,spec2);

    % verification
    clock = tic;
    [resPZ,x] = verifyPolyZonotope(NN,X0,spec);
    tComp = toc(clock); 

    disp(['Computation time (poly. zonotopes): ',num2str(tComp),'s']);

    clock = tic;
    [resCZ,x] = verify(NN,X0,spec);
    tComp = toc(clock); 

    disp(['Computation time (star sets): ',num2str(tComp),'s']);

    clock = tic;
    [res,x] = verifyZonotope(NN,X0,spec);
    tComp = toc(clock); 

    disp(['Computation time (zonotopes): ',num2str(tComp),'s']);

    % visualization
    points = evaluate(NN,randPoint(X0,100000)); 

    figure; hold on; box on;
    plot(spec(2).set,[1,2],'FaceColor',[0 0.8 0]);
    plot(spec(1).set,[1,2],'FaceColor','r');
    plot(points(1,:),points(2,:),'.k');
    
%------------- END OF CODE --------------