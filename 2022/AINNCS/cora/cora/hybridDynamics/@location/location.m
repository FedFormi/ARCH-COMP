classdef location
% location - constructor of class location
%
% Syntax:  
%    loc = location(invSet,trans,sys)
%    loc = location(name,invSet,trans,sys)
%
% Inputs:
%    name - name of the location
%    invSet - invariant set
%    trans - cell-array containing all transitions
%    sys - continuous dynamics
%
% Outputs:
%    loc - generated location object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: hybridAutomaton, transition

% Author:       Matthias Althoff
% Written:      02-May-2007 
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

properties (SetAccess = private, GetAccess = public)
    % name of the location
    name (1,:) {mustBeText} = 'location';

    % invariant set 
    invariant = [];

    % cell-array storing the transitions
    transition (:,1) cell = {};

    % system dynamics
    contDynamics {mustBeA(contDynamics,'contDynamics')} = contDynamics();
end

methods
    
    % class constructor
    function loc = location(varargin)

        % parse input arguments
        if nargin <= 2
            throw(CORAerror('CORA:notEnoughInputArgs',3));
        elseif nargin == 3
            invSet = varargin{1};
            loc.transition = varargin{2};
            loc.contDynamics = varargin{3};
        elseif nargin == 4
            loc.name = varargin{1};
            invSet = varargin{2};
            loc.transition = varargin{3};
            loc.contDynamics = varargin{4}; 
        else
            throw(CORAerror('CORA:tooManyInputArgs',4));
        end

        % elements of transition cell-array have to be transition objects
        if ~all(cellfun(@(x) isa(x,'transition'),loc.transition,'UniformOutput',true))
            throw(CORAerror('CORA:wrongInputInConstructor',...
                'All elements of transition have to be transition objects.'));
        end

        % convert invariant sets to polytopes if possible
        if (isnumeric(invSet) && isempty(invSet)) || ...
                isa(invSet,'mptPolytope') || isa(invSet,'levelSet')
            loc.invariant = invSet;
        else
            loc.invariant = mptPolytope(invSet);
        end
    end
end
end

%------------- END OF CODE --------------