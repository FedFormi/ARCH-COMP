function transSet = mergeTransitionSets(pHA,transList,locID,allLabels)
% mergeTransitionSets - computes the transition set of a combined location 
%    from the transition sets of the subcomponents
%
% Syntax:  
%    transSet = mergeTransitionSets(pHA,transList,locID,allLabels)
%
% Input:
%    pHA - parallelHybridAutomaton object
%    transList - A cell array containing the transition sets for all
%                 components
%    locID - IDs of the currently active locations
%    allLabels - information about all synchronization labels
%
% Outputs:
%    transSet - cell array containing the resulting transition sets
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author:       Johann Schoepfer, Niklas Kochdumper, Maximilian Perschl
% Written:      14-June-2018
% Last update:  16-March-2022 (MP, finalize synchronization implementation)
% Last revision:---

%------------- BEGIN CODE --------------

    % read out number of components
    numComp = length(pHA.components);

    % initialize resulting set of transitions
    transSet = {};
    % counter for number of transitions (cannot be known beforehand due to
    % synchronization labels)
    cnt = 1;
    
    % list of checked transition labels
    checkedLabels = {};

    % loop over all subcomponents
    for i = 1:numComp
        
        compTrans = transList{i};
        stateBind = pHA.bindsStates{i};

        % loop over all transitions for the current subcomponent
        for t = 1:length(compTrans)
            
            trans = compTrans{t};
            
            % prepare target location vector
            resultingTarget = locID;
                
            % handle non-synchronized transition (no synchronization label)
            if isempty(trans.syncLabel)
            
                % update the destination (target idx only changes in component i)
                resultingTarget(i) = trans.target;
                
                % convert each component transition to a transition on the PHA
                
                % transitions whose reset depends on input have to be
                % handled seperately for now
                if trans.reset.hasInput
                    transSet{cnt} = projectInputDependentTrans(pHA,...
                        trans,pHA.numStates,i,locID,resultingTarget);
                else
                    transSet{cnt} = projectHighDim(trans,pHA.numStates,...
                        stateBind,resultingTarget);
                end

                % increment counter of resulting transitions
                cnt = cnt + 1;
            
            % handle synchronized transition
            else
                label = trans.syncLabel;
                % skip over labels that have already been synchronized;
                % additionally, we can skip labels where there is not one
                % active instance per component containing that label
                if ismember(label,checkedLabels)
                    continue;
                else
                    checkedLabels = [checkedLabels; label];
                end
                
                % update target location of component i
                resultingTarget(i) = trans.target;

                % get transitions of other components with the same label:
                % start with index i+1 as all labels occurring in
                % components of index <= i have been checked already
                labelTransSet = trans;
                labelCompIdx = i;

                % we also save from which component they originate to infer
                % the correct state indices later
                labelStateIndices = {};
                labelStateIndices{1} = pHA.bindsStates{i};

                % loop over each component (sufficient to look through
                % components i+1:end because otherwise the label in the
                % current component would have been checked before)
                for j = i+1:length(transList)
                    transCurrentComp = transList{j};

                    % loop over transitions of each component
                    for k = 1:length(transCurrentComp)
                        currentTrans = transCurrentComp{k};

                        % check if label occurs 
                        if strcmp(currentTrans.syncLabel,label)
                            labelTransSet = [labelTransSet; currentTrans];
                            labelCompIdx = [labelCompIdx; j];
                            labelStateIndices = [labelStateIndices; pHA.bindsStates(j)];
                            % update target location of component j
                            resultingTarget(j) = currentTrans.target;
                            % once a transition with the relevant label has
                            % been found, we can stop scanning the current
                            % component (there should only be one outgoing
                            % transition per synchronization label in one
                            % location per component!)
                            break;
                        end

                    end
                end
                
                % the transition is only active if in all components where
                % the label occurs, of one these transitions is active
                % (the lenghty variable on the right-hand side computes the
                % number of components in which <label> occurs)
                if length(labelTransSet) ~= ...
                        length(unique(allLabels(strcmp({allLabels.name},label)).component))
                    continue;
                end
                
                
                % create a new transition from the transitions to be
                % synchronized, which models the relevant transitions being
                % executed simultaneously 
                
                
                % first, we project each transition to the higher dimension
                for j = 1:length(labelTransSet)
                    if labelTransSet(j).reset.hasInput
                        labelTransSet(j) = projectInputDependentTrans(pHA,...
                            labelTransSet(j),pHA.numStates,labelCompIdx(j),...
                            locID,resultingTarget);
                    else
                        labelTransSet(j) = projectHighDim(labelTransSet(j),...
                            pHA.numStates,labelStateIndices{j},resultingTarget);
                    end
                end
                % for nonlinear resets: all resulting reset functions are
                % defined using the full state as reset.stateDim, the
                % number of global inputs as reset.inputDim (so that
                % reset.hasInput is only true if there are global inputs to
                % composed system), and reset.f takes the extended state
                % vector (full state + global inputs) as input argument
                % for linear resets: states use the A matrix (where inputs
                % which are states from other components are already
                % resolved), global inputs use the B matrix
                
                % synchronize guards by intersection
                % note: might not work for all nonlinear guards
                resultingGuard = [];

                % note: empty guard sets can simply be skipped as they
                % represent the full subspace of the covered dimensions,
                % which is conveniently represented by the resulting zeros
                % -> the guard intersection will correctly yield
                % full-dimensional sets along these dimensions, thus we do
                % not require any further processing at this stage
                for j=1:length(labelTransSet)
                    if ~(isnumeric(labelTransSet(j).guard) && ...
                            isempty(labelTransSet(j).guard))
                        if isnumeric(resultingGuard) && isempty(resultingGuard)
                            % first full-dimensional guard enters here
                            resultingGuard = labelTransSet(j).guard;
                        else
                            % all subsequent full-dimensional guards
                            resultingGuard = resultingGuard & labelTransSet(j).guard;
                        end
                    end
                end
                

                % merge reset functions into one reset function
                resultingReset = synchronizeResets(labelTransSet,...
                    pHA.numStates,pHA.numInputs);
                
                % instantiate resulting transition
                transSet{cnt} = transition(resultingGuard,resultingReset,...
                    resultingTarget);

                % increment counter for number of transitions
                cnt = cnt + 1;
            end            
        end
    end
end

%------------- END OF CODE --------------
