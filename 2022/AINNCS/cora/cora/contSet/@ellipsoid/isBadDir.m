function res = isBadDir(E1,E2,L)
% isBadDir - checks if specified directions are bad directions for
%               Minkowski difference of E1 and E2
%
% Syntax:  
%    res = isBadDir(E1,E2,L)
%
% Inputs:
%    L  - (n x N) matrix, where n must be dim(E1)=dim(E2), and N is the
%           number of directions to check
%    E1 - ellipsoid object
%    E2 - ellipsoid object
%
% Outputs:
%    res - boolean (vector; length(res)=size(L,2))
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Victor Gassmann
% Written:      ???
% Last update:  10-June-2022
% Last revision:---

%------------- BEGIN CODE --------------
TOL = min(E1.TOL,E2.TOL);
[~,D] = simdiag(E1.Q,E2.Q,TOL);
r = 1/max(diag(D));
res = true(1,size(L,2));
for i=1:size(L,2)
    l = L(:,i);
    res(i) = sqrt(l'*E1.Q*l)/sqrt(l'*E2.Q*l) > r+TOL;
end

%------------- END OF CODE --------------