function E = convHull(E,S)
% convHull - computes an overapproximation of the convex hull of an
%    ellipsoid and another set representation
%
% Syntax:  
%    E = or(E,S)
%
% Inputs:
%    E - ellipsoid object
%    S - set (or cell array)
%
% Outputs:
%    E - ellipsoid object
%
% Example: 
%    E1 = ellipsoid.generateRandom('Dimension',2);
%    E2 = ellipsoid.generateRandom('Dimension',2);
%    E = convHull(E1,E2);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: ellipsoid/or

% Author:       Victor Gassmann
% Written:      13-March-2019
% Last update:  19-March-2021 (complete rewrite)
% Last revision:---

%------------- BEGIN CODE --------------

% simply call "or"
E = or(E,S,'o');

%------------- END OF CODE --------------