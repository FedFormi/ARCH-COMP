function E = andMptPolytope(E,P,varargin)
% andMptPolytope - Computes an inner-approximation or outer-approximation of
%    the intersection between an ellipsoid and a polytope
%
% Syntax:  
%    E = andHalfspace(E,P)
%    E = andHalfspace(E,P,mode)
%
% Inputs:
%    E - ellipsoid object
%    P - mptPolytope object
%    mode - (optional) type of approximation ('i': inner; 'o': outer)
%
% Outputs:
%    E - ellipsoid after intersection
%
% References:
%   [1] Kurzhanskiy, A.A. and Varaiya, P., 2006, December. Ellipsoidal
%       toolbox (ET). In Proceedings of the 45th IEEE Conference o
%       Decision and Control (pp. 1498-1503). IEEE.
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gassmann
% Written:      07-June-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% default mode
mode = 'o';

% parse input arguments
if nargin == 2 && ~isempty(varargin{1})
    if ~ischar(varargin{1}) || ~any(varargin{1}==['i','o'])
        throw(CORAerror('CORA:wrongValue','third',...
            "'i'(inner-approx) or 'o'(outer-approx)"));
    end
    mode = varargin{1};
end

% check if same dimension
if dim(E)~=dim(P)
    throw(CORAerror('CORA:dimensionMismatch','obj1',E,...
        'dim1',dim(E),'obj2',P,'dim2',dim(P)));
end

% halfspace rep of P (is computed if not there; expensive!)
A = P.P.A;
b = P.P.b;

% loop over each halfspace and compute using ellipsoidHalfspace
for i=1:size(A,1)
    E = andHalfspace(E,halfspace(A(i,:)',b(i)),mode);
end

%------------- END OF CODE --------------