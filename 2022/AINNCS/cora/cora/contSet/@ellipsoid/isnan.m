function res = isnan(E)
% isnan - checks if any value in the ellipsoid is NaN
%
% Syntax:  
%    res = isnan(E)
%
% Inputs:
%    E - ellipsoid object
%
% Outputs:
%    res - false
%
% Example: 
%    E = ellipsoid([5 7;7 13],[1;2]);
%    res = isnan(E)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Mark Wetzlinger
% Written:      01-June-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% NaN values are not possible by constructor
res = false;

%------------- END OF CODE --------------