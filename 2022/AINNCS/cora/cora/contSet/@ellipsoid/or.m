function E = or(E,S,varargin)
% or - overloads '|' operator to compute an inner-/outer-approximation of
%    the union of an ellipsoid and another set representation
%
% Syntax:  
%    E = or(E,S)
%    E = or(E,S,mode)
%
% Inputs:
%    E - ellipsoid object
%    S - set (or cell array)
%    mode - (optional) 'i' (inner-approximation) / 'o' (outer-approximation)
%
% Outputs:
%    E - ellipsoid object
%
% Example: 
%    E1 = ellipsoid.generateRandom('Dimension',2);
%    E2 = ellipsoid.generateRandom('Dimension',2);
%    E = E1 | E2;
%
% References:
%    Convex Optimization; Boyd
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Victor Gassmann
% Written:      09-March-2021 
% Last update:  15-March-2021
% Last revision:---

%------------- BEGIN CODE --------------

%% parsing & checking
[mode] = setDefaultValues({{'o'}},varargin{:});

% check input arguments
inputArgsCheck({{E,'att',{'ellipsoid'},{''}};
                {S,'att',{'cell','contSet','numeric'},{''}};
                {mode,'str',{'o','i'}}});

S = prepareSetCellArray(S,E);
if isempty(S)
    E = ellipsoid;
    return;
end

%% different unions
if isa(S{1},'double')
    V = cell2mat(reshape(S,[1,numel(S)]));
    if strcmp(mode,'o')
        E_p = ellipsoid.enclosePoints(V,'min-vol');
        E = orEllipsoidOA(E,{E_p});
        return;
    else
        P = mptPolytope.enclosePoints(V);
        E = ellipsoid(P,'i');
        return;
    end
end

if isa(S{1},'ellipsoid')
    if strcmp(mode','o')
        E = orEllipsoidOA(E,S);
        return;
    else
        S_ = [{E};S];
        [~,ii] = max(cellfun(@(ss)volume(ss),S_));
        E = S_{ii};
        return;
    end
end

if isa(S{1},'mptPolytope')
    throw(CORAerror('CORA:noops',E,S{1}));
end

% throw error for all other combinations
throw(CORAerror('CORA:noops',E,S{1}));

%------------- END OF CODE --------------