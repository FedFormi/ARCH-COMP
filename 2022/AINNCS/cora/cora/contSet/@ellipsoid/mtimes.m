function E = mtimes(A,E)
% mtimes - Overloaded '*' operator for the multiplication of a matrix with
%    an ellipsoid
%
% Syntax:  
%    E = mtimes(A,E)
%
% Inputs:
%    A - numerical matrix
%    E - ellipsoid object 
%
% Outputs:
%    E - ellipsoid object
%
% Example: 
%    E = ellipsoid([2.7 -0.2;-0.2 2.4]);
%    M = [1 0.5; 0.5 1];
% 
%    figure; hold on;
%    plot(E);
%    plot(M*E,[1,2],'r');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: plus

% Author:       Victor Gassmann
% Written:      13-March-2019 
% Last update:  15-October-2019
%               07-June-2022 (avoid construction of new ellipsoid object)
% Last revision:---

%------------- BEGIN CODE --------------

% parse input arguments
if ~isa(E,'ellipsoid')
    throw(CORAerror('CORA:wrongValue','second',"be of type 'ellipsoid'"));
elseif size(A,2)~=length(E.Q)
     if all(size(A)==1)
         A = A*eye(length(E.Q));
     else
         throw(CORAerror('CORA:dimensionMismatch','obj1',A,...
             'size1',size(A),'obj2',E,'dim2',dim(E)));
     end
end

% compute auxiliary value for new shape matrix
M = A*E.Q*A';
% make sure it is symmetric
M = 1/2*(M+M');

E.Q = M;
E.q = A*E.q;
%------------- END OF CODE --------------