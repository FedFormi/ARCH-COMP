function H = addedTermsHessian(pZ_vec,idp)
% addedTermsHessian - ???
%
% output: @(p,lambda) H(p,lambda); length(p) = length(idp)
% computes hessian(l1*g_11 + ... + l1*g_1N + l2*g_21+...+l2*g_2N+...)
% so l1*hessian(g_11) + ..., but more efficiently
%
% Syntax:  
%    H = addedTermsHessian(pZ_vec,idp)
%
% Inputs:
%    pZ_vec - ???
%    idp - ???
%
% Outputs:
%    H - ???
%
% Example: 
%    ---
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author:       Victor Gassmann
% Written:      22-December-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

Nt = dim(pZ_vec);

ind_cut = sum(pZ_vec.expMat,1)<=1 | all(pZ_vec.G==0,1);
if ~all(ind_cut)
    pZ_gsum = [];
    % ids for lambda
    idl = (max(abs(idp))+1:max(abs(idp))+Nt)';
    for i=1:Nt
        pZ_vec_i = project(pZ_vec,i);
        ind_cut_i = pZ_vec_i.G==0 | sum(pZ_vec_i.expMat,1)<=1;

        % remove <= linear generators
        if ~all(ind_cut_i)
            pZ_vec_i.G = pZ_vec_i.G(:,~ind_cut_i);
            pZ_vec_i.expMat = pZ_vec_i.expMat(:,~ind_cut_i);

            pZ_vec_i_ext = pZ_vec_i;
            mi = size(pZ_vec_i.expMat,2);
            pZ_vec_i_ext.expMat = [pZ_vec_i.expMat;ones(1,mi)];
            pZ_vec_i_ext.id = [pZ_vec_i.id;idl(i)];
            if ~isempty(pZ_gsum)
                pZ_gsum = exactPlus(pZ_gsum,pZ_vec_i_ext);
            else
                pZ_gsum = pZ_vec_i_ext;
            end
        end
    end

    ind_p_rem = ismember(idp,pZ_gsum.id);
    ind_l_rem = ismember(idl,pZ_gsum.id);
    idp_rem = idp(ind_p_rem);
    idl_rem = idl(ind_l_rem);
    H_ctr = hessianHandle(pZ_gsum,idp_rem,idl_rem);
    % reintroduce the "lost" p's, lambdas
    H = @(p,lambda) H_ctr(p(ind_p_rem),lambda(ind_l_rem));

else

    % linear (or constant) constraints
    H = @(p,lambda) zeros(length(idp));

end

%------------- END OF CODE --------------