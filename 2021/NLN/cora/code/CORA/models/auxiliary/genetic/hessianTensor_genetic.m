function Hf=hessianTensor_genetic(x,u)



 Hf{1} = sparse(10,10);

Hf{1}(6,1) = -1;
Hf{1}(1,6) = -1;


 Hf{2} = sparse(10,10);

Hf{2}(6,2) = -1;
Hf{2}(2,6) = -1;


 Hf{3} = sparse(10,10);

Hf{3}(6,1) = 1;
Hf{3}(1,6) = 1;


 Hf{4} = sparse(10,10);

Hf{4}(6,2) = 1;
Hf{4}(2,6) = 1;


 Hf{5} = sparse(10,10);



 Hf{6} = sparse(10,10);

Hf{6}(6,1) = -1;
Hf{6}(6,2) = -1;
Hf{6}(1,6) = -1;
Hf{6}(2,6) = -1;
Hf{6}(8,6) = -2;
Hf{6}(6,8) = -2;


 Hf{7} = sparse(10,10);



 Hf{8} = sparse(10,10);

Hf{8}(8,6) = -2;
Hf{8}(6,8) = -2;


 Hf{9} = sparse(10,10);

Hf{9}(8,6) = 2;
Hf{9}(6,8) = 2;
