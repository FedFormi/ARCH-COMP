/* Header file for inputs */
/* Created by: Kaushik */
/*      Date: 01.02.2021 */

#ifndef INPUT_HH_
#define INPUT_HH_

#include <float.h>

/* state space dim */
#define sDIM 2
#define iDIM 2

/* data types for the ode solver */
typedef std::array<double,sDIM> state_type;
typedef std::array<double,iDIM> input_type;
typedef std::array<double,2> state_xy_dim_type;

/* parallel or sequential implementation of computation of the
 transition functions */
const bool parallel = false;
int verbose = 2; /* verbose = 0: no verbosity, 1: print messages on the terminal, 2: save intermediate sets computed in the fixed point */
/* read transitions from file flag */
const bool RFF = true;
/* whether accelerated fixpoint is on or not */
const bool accl_on=true;
/* the memory bound (in terms of the maximum number of past iterations to be stored) in case acceleration is on */
const size_t M=10;
///* scaling */
//const double scale = 1;
/* sampling time */
const double tau = 0.08;
/* upper bounds on the noise which is considered to be centered around the origin */
const state_type W_ub = {0.06,0.06};
const double eps = 0.25; /* the radius of the reachable set from the state dimensions 3 and 4*/

/* lower bounds of the hyper rectangle */
double lb[sDIM]={0.0,0.0};
/* upper bounds of the hyper rectangle */
double ub[sDIM]={2.0,2.0};
/* grid node distance diameter */
double eta[sDIM]={0.08,0.08};

/* input space parameters */
double ilb[iDIM]={-1.0,-1.0};
double iub[iDIM]={1.0,1.0};
double ieta[iDIM]={0.1,0.1};

/* goal states */
/* add inner approximation of P={ x | H x<= h } form state space */
/* office */
double H1[4*sDIM]={-1, 0,
    1, 0,
    0,-1,
    0, 1
};
double g1[4] = {-0.2,0.7,-1.2,1.7};
/* kitchen */
double H2[4*sDIM]={-1, 0,
    1, 0,
    0,-1,
    0, 1};
double g2[4] = {-1.3,1.7,-0.3,0.7};
/* room 1 */
double H3[4*sDIM]={-1, 0,
    1, 0,
    0,-1,
    0, 1};
double g3[4] = {0.0,1.0,0.0,1.0};
/* room 2 */
double H4[4*sDIM]={-1, 0,
    1, 0,
    0,-1,
    0, 1};
double g4[4] = {0.0,0.6,0.0,0.6};
/* room 3 */
double H5[4*sDIM]={-1, 0,
    1, 0,
    0,-1,
    0, 1};
double g5[4] = {-1.0,2.0,-1.0,2.0};

///* static obstacle set */
//double HO[4*sDIM]={-1, 0,
//    1, 0,
//    0,-1,
//    0, 1};
//double ho1[4] = {-1.11,1.19,-0.9,1.7};
//double ho2[4] = {-1.1,1.2,-1.7,2.0};

auto check_intersection = [](state_xy_dim_type lb1, state_xy_dim_type ub1, state_xy_dim_type lb2, state_xy_dim_type ub2) -> bool {
    bool is_intersection=true; /* boolean flag which is positive if there is an intersection */
    /* if there is no intersection in either the first or the second state dimension, then there is no intersection */
    for (int i=0; i<2; i++) {
        if ((ub2[i]-lb1[i]<DBL_MIN) || (lb2[i]-ub1[i]>DBL_MIN)) {
            is_intersection=false;
            break;
        }
    }
    return is_intersection;
};

auto decomposition = [](input_type &u, state_type &z1, state_type &z2) -> state_type {
    state_type y;
    if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
        /* return the lower left corner of the reachable set approximation */
        y[0] = z1[0]+u[0]*tau;
        y[1] = z1[1]+u[1]*tau;
    } else { /* z1 is the upper bound and z2 is the lower bound */
        /* return the upper right corner of the reachable set approximation */
        y[0] = z1[0]+u[0]*tau;
        y[1] = z1[1]+u[1]*tau;
    }
    return y;
};

#endif /* INPUT_HH_ */
